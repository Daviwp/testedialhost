<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
});

/* Rota Envio dos dados para DB*/
Route::post('/cadastrar', function(Illuminate\Http\Request $request){

	$url = url('/');

	$contato = new App\Contato();
	$contato->nome = $request->get('nome');
	$contato->email = $request->get('email');
	$contato->telefone = $request->get('telefone');
	$contato->data = $request->get('data');
	$contato->cep = $request->get('cep');
	$contato->endereco = $request->get('endereco');
	$contato->bairro = $request->get('bairro');
	$contato->cidade = $request->get('cidade');
	$contato->estado = $request->get('estado');
 
	$contato->save();

	echo '
		
		<script>
	        alert("Cadastro realizado com sucesso!");
	        window.location.href = "'. $url .'";
	    </script>
		
	';

});