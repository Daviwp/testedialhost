<!DOCTYPE html>
<html>
<head>
    <title>Landing Page</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href="{{ asset('assets/style/main.css') }}" rel="stylesheet" type="text/css" >
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<body>

<div class="container">
    <!-- Form -->
    <div class="formulario col-md-5 col-lg-5">
      
      <h2>Fringilla Mollis Lorem</h2>
      <p>Donec ullamcorper nulla non <b>metus auctor fringilla</b>.</p>

      <form action="{{ asset('/cadastrar') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
          <input data-validation="required" data-validation-error-msg="Digite seu nome" type="text" id="nome" name="nome" class="form-control" placeholder="Nome">
        </div>

        <div class="form-group">
          <input data-validation="email" data-validation-error-msg="Digite seu email" type="text" id="email" name="email" class="form-control" placeholder="E-Mail">
        </div>

        <div class="form-group">
          <input id="campoTelefone" data-validation="required" data-validation-error-msg="Digite seu telefone" type="text" id="telefone" name="telefone" class="form-control" placeholder="Celular">
        </div>

        <div class="form-group">
          <input id="campoData" data-validation="required" data-validation-error-msg="Digite a data de nascimento" type="text" id="data" name="data" class="form-control" placeholder="Data de nascimento">
        </div>

        <div class="form-group">
          <input id="cep" data-validation="required" data-validation-error-msg="Digite seu cep" type="text" id="cep" name="cep" class="form-control" placeholder="CEP">
        </div>

        <div class="form-group">
          <input id="endereco" data-validation="required" data-validation-error-msg="Este campo é obrigatório" type="text" id="endereco" name="endereco" class="form-control" placeholder="Endereço">
        </div>

        <div class="form-group">
          <input id="bairro" data-validation="required" data-validation-error-msg="Este campo é obrigatório" type="text" id="bairro" name="bairro" class="form-control" placeholder="Bairro">
        </div>

        <div class="form-group">
          <input id="cidade" data-validation="required" data-validation-error-msg="Este campo é obrigatório" type="text" id="cidade" name="cidade" class="form-control" placeholder="Cidade">
        </div>

        <div class="form-group">
          <input id="estado" data-validation="required" data-validation-error-msg="Este campo é obrigatório" type="text" id="estado" name="estado" class="form-control" placeholder="Estado">
        </div>

        <button type="submit" class="btn btn-default btn-custom">Enviar</button>

      </form>
    </div>
    <!-- end form -->
    
    <footer>
      <div class="col-md-12">
        <p>Todos direitos reservados.</p>
      </div>
    </footer>

</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type='text/javascript' src='http://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js'></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script src="{{ asset('/assets/js/jquery.maskedinput.min.js') }}"></script>
<script>

  $.validate({});
  $("#campoData").mask("99/99/9999");
  $("#campoTelefone").mask("(99) 99999-9999");

  $("#cep").change(function(){
      var cep_code = $(this).val();
      if( cep_code.length <= 0 ) return;
      $.get("http://apps.widenet.com.br/busca-cep/api/cep.json", { code: cep_code },
         function(result){
            if( result.status!=1 ){
               alert(result.message || "Houve um erro desconhecido");
               return;
            }
            $("input#cep").val( result.code );
            $("input#endereco").val( result.address );
            $("input#bairro").val( result.district );
            $("input#cidade").val( result.city );
            $("input#estado").val( result.state );
         });
   });
</script>


</body>
</html>